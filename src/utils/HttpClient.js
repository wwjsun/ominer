//http://visionmedia.github.io/superagent/
import request from 'superagent'
import router from '../router/index'
import $ from 'jquery'
import Vue from 'vue';
import loadding from '../pluging/loadding/loadding'

const net = 'pro_net';
// const net = 'dev_net';
// const net = 'test_net';

const baseUrl = {
    dev_net: 'http://127.0.0.1:8080/',
    pro_net: 'http://www.cminerok.com/',
};

const fileBaseUrl = {
    dev_net: 'http://127.0.0.1:8080/',
    pro_net: 'http://www.cminerok.com/',
}

const requestResultCode = {
    '1': '请求成功',
    '2': '系统错误'

}

function getUrl(path) {
    if (path.startsWith('http')) {
        return path;
    }
    return baseUrl[net] + path;
}

const errorHandler = (data) => {   
    alert(data.message, '系统提示');
}
let ajaxList = [];

const HttpClient = {
    fileUrl: fileBaseUrl[net],
    get: (path, query = {}, hideloading = false) => new Promise((resolve, reject) => {
        let id = parseInt(Math.random() * 999999999999).toString()
        ajaxList.push(id);        
        if(!hideloading){
            loadding.show();
        }
        query['_'] = Math.random();      
        var req = request
            .get(getUrl(path))
            .query(query)
            .end((err, res) => {                
                ajaxList.splice(ajaxList.indexOf(id), 1);
                if(ajaxList.length < 1){
                    loadding.close();
                }
                if(res.body.code == 1){
                    resolve(res.body);
                } else {
                    errorHandler(res.body)
                    reject(res.body);
                }
            });
    }),    
    post: (path, formdata, hideloading = false) => new Promise((resolve, reject) => {
        let id = parseInt(Math.random() * 999999999999).toString()
        ajaxList.push(id);
        if(!hideloading){
            loadding.show();
        }
        path = getUrl(path);         
        request
            .post(path)
            .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
            .send(formdata)
            .end((err, res) => {
                ajaxList.splice(ajaxList.indexOf(id), 1);
                if(ajaxList.length < 1){
                    loadding.close();
                }
                if(res.body.code == 1){
                    resolve(res.body);
                } else {
                    errorHandler(res.body)
                    reject(res.body);
                }                    
            });
    }),
    delete: (path, formdata, query) => new Promise((resolve, reject) => {
        loadding.show();
        path = getUrl(path);
        request
            .delete(path)
            .set('Content-Type', 'application/json; charset=UTF-8')
            .query(query)
            .send(formdata)
            .end((err, res) => {
                loadding.close();
                if(res.body.code == 1){
                    resolve(res.body);
                } else {
                    errorHandler(res.body)
                    reject(res.body);
                }                     
            });
    })    
};

export default HttpClient;