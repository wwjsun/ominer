import $ from 'jquery'

let indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.msIndexedDB;
if(!indexedDB)
{
    $.alert("手机存储容量不支持");
}

let openDB = (tb) => {
    return new Promise((resolve, reject) => {
        var idbRequest = window.indexedDB.open('mine', 1);
        idbRequest.onerror = function(e){
            reject(e.currentTarget.error.message);
        };
        idbRequest.onsuccess = function(e){
            var db = e.target.result;
            resolve(db)
        };
        idbRequest.onupgradeneeded = function(e){
            var db = e.target.result;
            if(!db.objectStoreNames.contains(tb)){
                var objectStore = db.createObjectStore(tb, {
                    keyPath:"id",
                    autoIncrement: true
                });
            }

            resolve(db);
        };
    })
}

// openDB();
// openDB('aa')

export default {
    async insert(tb,data){
        let db = await openDB(tb);
        let transaction = db.transaction(tb, 'readwrite');
        let store = transaction.objectStore(tb);
        store.add(data);
    },

    async select(tb,key){
        let db = await openDB(tb);
        let transaction = db.transaction(tb, 'readwrite');
        let store = transaction.objectStore(tb);        
        let request = key ? store.get(key) : store.openCursor();

        let data = {};
        
        return new Promise((resolve, reject) => {
            request.onsuccess = (e) => {
                let cursor = e.target.result;
                if(cursor){
                    if(key){
                        // data[cursor.address.toLowerCase()] = cursor;
                        data = cursor;
                        resolve(cursor)
                    } else {
                        data[cursor.value.address.toLowerCase()] = cursor.value;
                        cursor.continue();
                    }
                } else {
                    resolve(data);
                }
            }            
        })
    },

    async update(tb,key, newdata = {}){
        let db = await openDB(tb);
        let transaction = db.transaction(tb, 'readwrite');
        let store = transaction.objectStore(tb);  
        let request = store.get(key);
        return new Promise((resolve, reject) => {
            request.onsuccess = (e) => {
                let data = e.target.result;
                if(!data){
                    resolve(undefined);
                    return false;
                }
                for(let key in newdata){
                    data[key] = newdata[key];
                }
                
                store.put(data);

                resolve(data);
            }            
        })
    },

    async delete(tb,key){
        let db = await openDB(tb);
        let transaction = db.transaction(tb, 'readwrite');
        let store = transaction.objectStore(tb);  
        let request = store.delete(key);
    }
}