import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../vuex/store'
import db from '../utils/db'
import $ from 'jquery'

import MainComponent from '../components/main/main.vue'

import ComputComponent from '../components/comput/comput.vue'
import MineComponent from '../components/mine/mine.vue'
import UserComponent from '../components/user/user.vue'
import ActivateComponent from '../components/activate/activate.vue'



// 子页面路由
import ChangepswComponen from '../components/subpage1/changePsw.vue'
import ResetPwd from '../components/subpage1/resetPwd.vue'

import walletManageComponent from '../components/subpage1/wallet_manage'

import DepositComponent from '../components/subpage1/deposit.vue'
import MDepositComponent from '../components/subpage1/depositRight.vue'

import httpClient from '../utils/HttpClient.js'

import MoreComponent from '../components/subpage1/more.vue'
import privateKeyComponent from '../components/subpage1/privateKey.vue'

import FetchDetailComponent from '../components/fetchdetail/fetchdetail.vue'




Vue.use(VueRouter)

const router = new VueRouter({
  // mode: 'history',
  linkActiveClass: 'router-link-actived',
  routes: [{
      path: '/',
      name: 'main',
      component: MainComponent,
      redirect: 'comput',
      children: [{
        path: '/comput',
        name: 'comput',
        component: ComputComponent,
        meta: {
          index: 1,
          level: 1,
          keepAlive: true
        }
      }, {
        path: '/mine',
        name: 'mine',
        component: MineComponent,
        meta: {
          index: 2,
          level: 1,
          keepAlive: false
        }
      }, {
        path: '/user',
        name: 'user',
        component: UserComponent,
        meta: {
          index: 3,
          level: 1,
          keepAlive: false
        }
      }]
    }, {
      path: '/changepsw',
      name: 'changepsw',
      component: ChangepswComponen

    },
    {
      path: "/resetPwd",
      name: "resetPwd",
      component: ResetPwd
    }, {
      path: '/activate',
      name: 'activate',
      component: ActivateComponent
    },

    {
      path: '/deposit',
      name: 'deposit',
      component: DepositComponent,
    }, {
      path: '/more',
      name: 'more',
      component: MoreComponent
    }, {
      path: '/Mdeposit',
      name: 'Mdeposit',
      component: MDepositComponent
    }, {
      path: '/wallet_manage',
      name: 'wallet_manage',
      component: walletManageComponent
    }, {
      path: '/privateKey',
      name: 'privateKey',
      component: privateKeyComponent
    }, {
      path: '/fetchdetail/:id',
      name: 'fetchdetail',
      component: FetchDetailComponent
    }
  ]
})

// router.beforeEach((to, from, next)=> {
//     // db.insert('imei',{'id':1,'imei':'123456'})
//     // 如果返回为false，表示已激活，路由跳转到comput页面页面
//     db.select('imei',1).then((result) => {
//         if(result.id){
//             store.state.imei = result.imei;
//             httpClient.get("api/user/isActivated",{
//                 "imei":store.state.imei 
//             }).then((res)=>{
//                 if(res.data){
//                     if(to.path == '/activate'){
//                         next({path:'/comput'});
//                     }
//                     next();
//                     // 如果返回为false，路由跳转到激活页面
//                 } else{
//                     function plusReady(){
//                         store.state.imei = plus.device.imei;
//                         if(to.path != '/activate'){
//                             next({path:'/activate'}); 
//                         }
//                         next();
//                     };
//                     if(window.plus){
//                         plusReady();
//                     }else{
//                         document.addEventListener("plusready",plusReady,false);
//                     }
//                 } 
//             }).catch((res)=>{
//                 function plusReady(){
//                     store.state.imei = plus.device.imei;
//                     if(to.path != '/activate'){
//                         next({path:'/activate'}); 
//                     }
//                     next();
//                 };
//                 if(window.plus){
//                     plusReady();
//                 }else{
//                     document.addEventListener("plusready",plusReady,false);
//                 }
//             })
//         }else{ //如果在indexdb查询imei，但是没有imei，就获取手机的imei，跳转到激活页面
//             function plusReady(){
//                 db.insert('imei',{'id':1,'imei':plus.device.imei})
//                 store.state.imei = plus.device.imei;
//                 httpClient.get("api/user/isActivated",{
//                     "imei":store.state.imei 
//                 }).then((res)=>{
//                     if(res.data){
//                         if(to.path == '/activate'){
//                             next({path:'/comput'});
//                         }
//                         next();
//                         // 如果返回为false，路由跳转到激活页面
//                     } else{
//                         function plusReady(){
//                             store.state.imei = plus.device.imei;
//                             if(to.path != '/activate'){
//                                 next({path:'/activate'}); 
//                             }
//                             next();
//                         };
//                         if(window.plus){
//                             plusReady();
//                         }else{
//                             document.addEventListener("plusready",plusReady,false);
//                         }
//                     } 
//                 }).catch((res)=>{
//                     function plusReady(){
//                         store.state.imei = plus.device.imei;
//                         if(to.path != '/activate'){
//                             next({path:'/activate'}); 
//                         }
//                         next();
//                     };
//                     if(window.plus){
//                         plusReady();
//                     }else{
//                         document.addEventListener("plusready",plusReady,false);
//                     }
//                 })
//             };
//             if(window.plus){
//                 plusReady();
//             }else{
//                 document.addEventListener("plusready",plusReady,false);
//             }
//             // store.state.imei = '123456';
//             // if(to.path != '/activate'){
//             //     next({path:'/activate'}); 
//             // }
//             // next();
//         }
//         //如果在indexdb查询imei出错就获取手机的imei，跳转到激活页面
//     }).catch((err) => { 
//         function plusReady(){
//             db.insert('imei',{'id':1,'imei':plus.device.imei})
//             store.state.imei = plus.device.imei;
//             httpClient.get("api/user/isActivated",{
//                 "imei":store.state.imei 
//             }).then((res)=>{
//                 if(res.data){
//                     if(to.path == '/activate'){
//                         next({path:'/comput'});
//                     }
//                     next();
//                     // 如果返回为false，路由跳转到激活页面
//                 } else{
//                     function plusReady(){
//                         store.state.imei = plus.device.imei;
//                         if(to.path != '/activate'){
//                             next({path:'/activate'}); 
//                         }
//                         next();
//                     };
//                     if(window.plus){
//                         plusReady();
//                     }else{
//                         document.addEventListener("plusready",plusReady,false);
//                     }
//                 } 
//             }).catch((res)=>{
//                 function plusReady(){
//                     store.state.imei = plus.device.imei;
//                     if(to.path != '/activate'){
//                         next({path:'/activate'}); 
//                     }
//                     next();
//                 };
//                 if(window.plus){
//                     plusReady();
//                 }else{
//                     document.addEventListener("plusready",plusReady,false);
//                 }
//             })
//         };
//         if(window.plus){
//             plusReady();
//         }else{
//             document.addEventListener("plusready",plusReady,false);
//         }
//     });  
// })

router.afterEach((to, from) => {
  window.setTimeout(() => {
    store.commit('appModule/update_direction', {
      direction: ''
    })
  }, 100)

})

export default router
