import Vue from 'vue'
import Vuex from 'vuex'


import mheaderModule from '../components/mheader/mheader'
import indexdb from '../utils/db'



Vue.use(Vuex)

const state = {
    isShow:0,
    imei:'',
    currversion: '1.0',
    isStart:0,
    updateUrl:''
}
let appModule = {
    namespaced: true,
    state: {
        direction: '',
        currversion: '1.0'
    },
    mutations: {
        update_direction(_state, _payload){
            _state.direction = _payload.direction
        }
    }
}
const store = new Vuex.Store({  
    modules: {
        appModule,
        mheaderModule
    },
    state
})

export default store