const state = {
    backEvent: null
}

const mutations = {
    setBack(_state, _payload){
        _state.backEvent = _payload.event;
    },

    goBack(_state, _payload){
        this.commit('appModule/update_direction', {direction: 'back'})
        _state.backEvent();
    }
}

export default {
    namespaced: true,
    state,
    mutations
}