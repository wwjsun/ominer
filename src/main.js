import './assets/font-awesome-4.7.0/css/font-awesome.css'
import './assets/bootstrap/css/bootstrap.min.css'
import './assets/jquery-confirm/jquery-confirm.css'

import './assets/animate/animate.css'
import './assets/common/common.scss'
import './assets/iconfont/iconfont.css';

import './assets/jquery-confirm/jquery-confirm.js'
import './assets/h5p/common.js'

import $ from 'jquery'

import router from './router'
import toast from './pluging/toast/toast'
import store from './vuex/store'
import { mapGetters } from 'vuex';




import Vue from 'vue'
import App from './App.vue'

Vue.prototype.$jq = $;

Vue.use(toast)

new Vue({
	el: '#app',
    data : {
    },
    router,
	store,
	render: h => h(App)
})
